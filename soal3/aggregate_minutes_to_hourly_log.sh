#!/bin/bash

# Get the current date to search
dt=$(date '+%Y%m%d%H');
us=$(whoami)
# use cat to search for a file that contains the dt string and appends it to a new file called temp_agg
cat /home/$us/bin/*metrics_$dt*.log > /home/$us/bin/temp_agg_$dt.log

# example command for the separation
# from argument 2 onwards it is the numbers itself
# awk  '/^Mem/ {print $1" "$2" "$3" "$4" "$5" "$6" "$7}' /home/$us/temp_agg_$dt.log | sort -nk2,2 | tail -1

# Main goal :
# We separate each arguments using AWK, Sort it, and print the tail/head that we want

# we use tail because the max is at the bottom
echo ========== >> /home/$us/bin/metrics_agg_$dt.log
echo $dt >> /home/$us/bin/metrics_agg_$dt.log
echo ====== >> /home/$us/bin/metrics_agg_$dt.log
echo MAX >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $3}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $4}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $5}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $6}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $7}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {print $3}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {print $4}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/home/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/home/ {print $1}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log


# we use head because the smallest is on the top
echo ====== >> /home/$us/bin/metrics_agg_$dt.log
echo MIN >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $3}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $4}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $5}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $6}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {print $7}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {print $3}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {print $4}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/home/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/home/ {print $1}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log

# We separate each arguments using AWK, total keeps track the total of each arguments and count calculates the total number
echo ====== >> /home/$us/bin/metrics_agg_$dt.log
echo AVG >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {total += $2; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {total += $3; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {total += $4; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {total += $5; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {total += $6; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Mem/ {total += $7; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {total += $2; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {total += $3; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/^Swap/ {total += $4; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
awk  '/home/ {print $2}' /home/$us/bin/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
awk  '/home/ {total += $1; count++ } END {print total/count}' /home/$us/bin/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
