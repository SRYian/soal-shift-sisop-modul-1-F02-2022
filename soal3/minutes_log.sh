#!/bin/bash

# 1 problem with installing manually
# Innate cron/auto run cron from this bash script
# problem: date doesnt change, so it all comes into 1 file 

# #write out current crontab
# crontab -l > mycron
# #echo new command into cron file

# echo "* * * * * free -m >> /home/$USER/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log" >> mycron
# echo "* * * * * du -sh $HOME >> /home/$USER/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log" >> mycron
# echo "* * * * * /bin/bash /home/osboxes/Documents/Gitlab/soal-shift-sisop-modul-1-F02-2022/soal3/minutes_log.sh" >> mycron

# #install new cron file
# crontab mycron
# rm mycron

# 2(Works with external cron)
# Alternative without cron  S
# this uses cron externally(aka DIY :v) to run this file
dt=$(date +\%Y\%m\%d\%H\%M\%S);
us=$(whoami)
free -m -t >> /home/$us/bin/metrics_$dt.log
du -sh /home/$us >> /home/$us/bin/metrics_$dt.log