# soal-shift-sisop-modul-1-F02-2022

# Group
1. **Billy Brianto**(5025201080)
2. Amsal Herbert(5025201182)
3. Aqil Ramadhan Hadiono(5025201261)

# Number 1

Bagian ini memiliki 2 file:
- main.sh
- register.sh

## register.sh
File ini berguna untuk user creation


Yang pertama akan dilakukan adalah
```bash
# check if file, exist
if [ -f "$fileuser" && "$filelog" ]
then 
  echo ada
else
    mkdir users
    touch $fileuser
    touch $filelog
fi
```
berguna untuk mengecek dan membuat file dan folder log dan user

kemudian, user akan diprompt untuk menginput username.
username yang diinput akan dicek menggunakan
```bash
grep -q $username "$fileuser"
```
jika user telah dikonfirmasi, baru prompt password akan muncul

Untuk konfirmasi password, password akan dicek dengan syarat
- Memiliki angka
- Memiliki huruf kapital dan kecil
- Minimal length 8 karakter
- Tidak sama dengan username

```bash
if  [[ $pass =~ [0-9] ]] && [[ $pass =~ [a-z] ]] && [[ $pass =~ [A-Z] ]] && [[ "$LEN" -ge 8 ]] && [[ $pass != $username ]]; 
    then        
        # Write to file that the username and password has been registered
        echo "REGISTRATION SUCCESS!!"
        echo "$dt REGISTER:INFO User $username registered successfully" >> "$filelog"
        #UNSAFE to do in regular practice, write the username and password to a file, separated by -
        #UNENCRYPTEDBTW
        echo "$username-$pass" >> "$fileuser"
    else
        #If the password doesnt meet the requirement
        echo "Password must be alphanumeric and a minimum of 8 chacarters"
    fi 
```

jika sudah, list user akan disimpan sebagai string berisi

```
username-pass
```

## main.sh
File ini adalah file utama dari sistem user

Komponen Utama:
- main login prompt
- function runprog

Dalam main login prompt, user akan ditujukan untuk login.
Untuk mengkonfirmasi, string akan diformat sebagai username-pass dan akan dicari user yang memiliki identitas yang sama menggunakan grep.
```
grep -q "$username-$pass" "$fileuser"
```

Setelah login, terdapat select loop untuk dua buah command
1. att
2. dl n

### att
```bash
total=$( grep -o "$username" "$filelog" | wc -l )
```
Command ini menghitung total percobaan login
untuk menghitungnya, cukup hitung username pada logfile
dan kurangi 1 karena semua attempt login akan berisi string username. Pengurangan 1 bertujuan untuk mengeluarkan log register(hanya 1 kali) dari perhitungan.  

### dl
Command ini bertujuan untuk mendownload dan zip file
Setelah command ini dijalankan, user harus menginput berapa file sebanyak n yang akan didownload.
Untuk memudahkan, proses download dan zip dipindahkan ke function runprog

### runprog
```bash
-f "$foldername.zip"
```
Fungsi utama untuk download dan zip file
Setelah menyiapkan semua variabel, akan dicek terlebih dahulu jika zip sudah ada menggunakan -f.

```bash
# Create a new folder with mkdir
        mkdir $foldername
        #start file numbering from 0 to n using a for loop
        for ((num=1; num<=$n; num++))
        do
            #download the file using curl
            curl https://loremflickr.com/320/240 -L -o $foldername/PIC_"0$num".jpeg 
        done
        # zip with the password accordingly
        zip -P $password -r $foldername.zip $dt\_$username
```
Jika tidak ada, akan dibuat folder dengan nama yang sesuai
dan file akan didownload, pada kaus ini numbering file akan dimulai dari 1. Kemudian folder akan dizip menggunakan password user.

```bash
        unzip -P $password $foldername.zip
        # resume the loop to add
        start=$(ls $foldername| wc -l)
        # continue file numbering, ex: if we left off at 05, next will be 06         
        for ((num=$start; num<=($n+$start); num++))
        do
            #download the file using curl and name it according to the format
            #-L to handle web redirect and -o to save as
            curl https://loremflickr.com/320/240 -L -o $foldername/PIC_"0$num".jpeg 
        done
        #rezip with the password accordingly
        # -P to add password, -r to recursively go into the child folders         
        zip -P $password -r $foldername.zip $dt\_$username
```
Sebaliknya jika ada, folder akan diunzip dan file tambahan akan didownload, pada kasus ini numbering file akan dilanjutkan dan folder akan dizip kembali. Untuk mengetahui numbering, cukup hitung banyak isi folder yang telah diunzip.


# Number 2
Bagian ini memiliki 1 file:
- soal2_forensic_dapos.sh

### Folder
```bash
# make a new directory for the frensic log 
if [ -f "$filerata" && "$filelog" ]
then 
  echo "file forensic ada"
else
    mkdir forensic_log_website_daffainfo_log
fi
```
Untuk menyiapkan semua file dan folder, yang pertama dilakukan adalah mengecek dan membuat folder tempat hasil.
Dari file log, terlihat bahwa tiap argumen terpisah dengan :
Untuk menangani ini, digunakan awk. Dengan awk, semua argumen dapat dipisahkan dengan token :.
### Rata-rata Request
```bash
time=$(awk -F[:\] '{ print $3}' ./log_website_daffainfo.log | tail -1)
awk -v a="$time" -F[:\ ] '{ count++}END{print "Rata-rata serangan adalah sebanyak " (count-1)/a " requests per jam"}' ./log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt
```

Untuk mencari rata-rata, yang diperlukan adalah jumlah akses dan jam akses.
Untuk jumlah jam, argumen 3 akan diambil dan angka paling besar akan diambil menggunakan command.
```bash
tail -1
```
variabel ini akan digunakan kembali untuk average. Untuk mencari jumlah akses digunakan variabel count yang akan menghitung tiap baris. Untuk menghapus template petunjuk 
```
"IP":"Date":"Request":"Status Code":"Content Length":"User Agent"
```
maka count akan dikurangi 1. Akhirnya, average dihitung dengan membagi count dengan jumlah wakt

### IP yang paling banyak melakukan request
```bash
awk -F[:\ ] '{ip[$1]++} END { for (i in ip) print "IP yang paling banyak mengakses server adalah: " i " sebanyak " ip[i]"  requests"}' ./log_website_daffainfo.log | sort -nk10,10 | tail -1 >> forensic_log_website_daffainfo_log/result.txt
```
Untuk mencari ip yang paling banyak mengakses server digunakan array dengan token pertama sebagai element. Dengan cara ini, akan muncul jumlah semua ip dan jumlah munculnya. 

```bash
sort -nk10,10 | tail -1
```
Output ini kemudian akan disort sesuai jumlah muncul dan yang paling banyak akan diambil menggunakan command tail.
> -nk10, 10 digunakan untuk sort dengan acuan argumen kesepuluh

### Jumlah request user agent "curl"
```bash
awk -F[:\ ] '/curl/ {++n} END {print "Ada " n " requests yang menggunakan curl sebagai user-agent"}' ./log_website_daffainfo.log | sort >> forensic_log_website_daffainfo_log/result.txt
```
Untuk mencari user curl, setelah memisahkan argumen dengan :, substring curl akan dicari. Jika ditemukan, akan variabel n akan ditambah 1.

### IP yang mengakses pada jam 2 pagi pada tanggal 22
```bash
awk -F[:\ ] '{if ($3 == "02") print $1}' ./log_website_daffainfo.log | sort -u >> forensic_log_website_daffainfo_log/result.txt
```
Pencarian untuk mencari ip yang mengakses pada jam 2 pagi tanggal 22 dapat dilakukan dengan mencari string 02 pada argumen ketiga saja. Hal ini mungkin karena semua akses yang terdapat dalam log terjadi pada hari yang sama.

# Number 3
Bagian ini memiliki 2 file dan 1 file lokal:
- minutes_log.sh
- aggregate_minutes_to_hourly_log.sh
- crontab(tidak ada pada repo)

### minutes_log.sh
File ini digunakan untuk menjalankan dua command, yaitu free dan du.
Pertama, dilakukan format date untuk nama file. Kemudian, kedua command tersebut dijalankan dan hasilnya dioutput ke directory 
```bash
us=$(whoami)
/home/$us/metrics_$dt.log
```
dimana variabel us merupakan user sat ini dan dt merupakan format waktu


### aggregate_minutes_to_hourly_log.sh
Untuk aggregasi file, dapat digunakan command
```bash
us=$(whoami)
cat /home/$us/bin/*metrics_$dt*.log > /home/$us/bin/temp_agg_$dt.log
```
command ini akan mencari file dengan substring tanggal(variabel dt) dan akan menggabungkan isi file yang ditemukan kedalam file baru. Isi file baru ini merupakan output raw dari command

Setelah itu, terdapat 3 kasus yang harus ditangani:
1. MIN
2. MAX
3. AVG

Untuk menangani semua kasus tersebut digunakan awk untuk memisahkan masing-masing baris dengan mencari substring Mem, Swap atau home. Setelah itu output disesuaikan.
Untuk MAX, yang diambil adalah tail(elemen terakhir) karena hasil terbesar berada pada tail. Sebaliknya berlaku untuk MIN(digunakan head). Untuk AVG, dapat digunakan variabel total untuk menghitung argumen dan count digunakan untuk jumlah nomor. Kemudian, semua output tersebut akan diredireksi ke file aggregation 

Perbandingan 3 command awk untuk satu bagian(masing-masing satu untuk tiap aspek)
1. MIN
```bash
awk  '/^Mem/ {print $2}' /home/$USER/temp_agg_$dt.log | sort -nk2,2 | tail -1 >> /home/$us/bin/metrics_agg_$dt.log
```
2. MAX
```bash
awk  '/^Mem/ {print $2}' /home/$USER/temp_agg_$dt.log | sort -nk2,2 | head -1 >> /home/$us/bin/metrics_agg_$dt.log
```
3. AVG
```bash
awk  '/^Mem/ {total += $2; count++ } END {print total/count}' /home/$USER/temp_agg_$dt.log >> /home/$us/bin/metrics_agg_$dt.log
```


### crontab
Tentunya untuk menjalankan command tiap menit secara manual tidak mungkin. Oleh sebab itu, dibutuhkan crontab.
Untuk membuka crontab, dijalankan command
```bash
crontab -e
```
dengan command ini crontab dapat diedit. Dengan itu, file aggegate dan minute log dapat diotomasi setiap menit dengan menuliskan:
```bash
SHELL=/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin
# untuk minute log, jalan tiap menit
* * * * * /bin/bash /home/osboxes/Documents/Gitlab/soal-shift-sisop-modul-1-F02-2022/soal3/minutes_log.sh
# untuk aggregate, jalan tiap jam
0 * * * * /bin/bash /home/osboxes/Documents/Gitlab/soal-shift-sisop-modul-1-F02-2022/soal3/aggregate_minutes_to_hourly_log.sh
```
Pada file crontab juga terdapat path untuk SHELL dan PATH. Hal ini dilakukan agar crontab dapat mengenal environment variable dan dapat menjalankan script bash

> Lokasi path untuk tiap orang mungkin berbeda
> Keharusan variabel PATH dan SHELL sebelum crontab berbeda dari tiap komputer


# Masalah dan Kendala
### Nomor 1
![nomer1](https://gitlab.com/uploads/-/system/personal_snippet/2266008/971b8fd82ea2647af7d6f5c169a7cf80/nomer1.jpg)
- Bagian unzip dan zip yang sulit untuk dilakukan, akhirnya dapat dijalankan dengan command.
```bash
zip -P $password -r $foldername.zip $dt\_$username
```
- Pemikiran untuk melanjutakan penomoran nama file, hal ini diselesaikan dengan menghitung file dalam folder dengan command.
```bash
start=$(ls $foldername| wc -l)
```

### Nomor 2
![nomer2](https://gitlab.com/uploads/-/system/personal_snippet/2266008/c6f794b531376c4c22b5a24d43287631/nomer2.jpg)
```bash
"IP":"Date":"Request":"Status Code":"Content Length":"User Agent"
```
- Pemisahan token AWK kurang efektif, sehingga pemisahan dengan token : terhambat pada Date
Hal ini disebabkan oleh format date yang menggunakan : juga, diselesaikan dengan mencari token yang benar.

### Nomor 3
![nomer3_1](https://gitlab.com/uploads/-/system/personal_snippet/2266008/16ac95c3cee6221b10fad01975cac345/nomer3_1.jpg)
![nomer3_2](https://gitlab.com/uploads/-/system/personal_snippet/2266008/5cc6e135489baf10d5381fb5453d9cdb/nomer3_2.jpg)
- Format output cari command free -m dan du sh sulit dipisahkan dengan AWK, sehingga memerlukan beberpa command.
- Kekurangan dua cariabel PATH dan SHELL, hal ini menyebabkan crontab *tidak dapat dijalankan*.



